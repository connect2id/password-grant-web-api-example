package com.nimbusds.openid.connect.provider.spi.grants.password.webapi.handler.example;


import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.http.JakartaServletUtils;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;


/**
 * Receives password grant requests.
 */
public class HandlerServlet extends HttpServlet {


	/**
	 * The logger.
	 */
	private static final Logger LOG = LogManager.getLogger("MAIN");


	/**
	 * The password grant grantHandler.
	 */
	private Handler grantHandler;


	@Override
	public void init(ServletConfig config)
		throws ServletException {

		super.init(config);

		var apiAccessToken = new BearerAccessToken(config.getInitParameter("apiAccessToken"));
		String ldapHost = config.getInitParameter("ldapHost");
		int ldapPort = Integer.parseInt(config.getInitParameter("ldapPort"));
		String ldapDnTemplate = config.getInitParameter("ldapDnTemplate");
		Scope allowedScopeValues = Scope.parse(config.getInitParameter("allowedScopeValues"));

		grantHandler = new Handler(apiAccessToken, ldapHost, ldapPort, ldapDnTemplate, allowedScopeValues);

		LOG.info("Started password grant handler: ldapHost={} ldapPort={} ldapDnTemplate={} allowedScopeValues={}",
			ldapHost, ldapPort, ldapDnTemplate, allowedScopeValues);
	}


	@Override
	public void doPost(final HttpServletRequest servletRequest, final HttpServletResponse servletResponse)
		throws ServletException {
		
		try {
			HTTPRequest httpRequest = JakartaServletUtils.createHTTPRequest(servletRequest);
			HTTPResponse httpResponse = grantHandler.process(httpRequest);
			JakartaServletUtils.applyHTTPResponse(httpResponse, servletResponse);

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new ServletException(e.getMessage(), e);
		}
	}
}
