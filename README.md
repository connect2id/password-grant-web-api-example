# Example Web API-Based Password Grant Handler

Copyright (c) Connect2id Ltd., 2014 - 2024


README

Example implementation for a Connect2id server web hook for handling resource
owner password credentials grants (see RFC 6749, section 4.3).

For more information:

 * <https://connect2id.com/products/server/docs/integration/password-grant-handler>
 * <https://bitbucket.org/connect2id/password-grant-web-api>
 * <https://bitbucket.org/connect2id/server-sdk>

Questions or comments? Email support@connect2id.com
