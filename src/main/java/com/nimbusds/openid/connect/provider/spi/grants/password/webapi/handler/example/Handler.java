package com.nimbusds.openid.connect.provider.spi.grants.password.webapi.handler.example;


import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.openid.connect.provider.spi.grants.*;
import com.nimbusds.openid.connect.provider.spi.grants.handlers.web.password.PasswordGrantHandlerRequest;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import com.nimbusds.openid.connect.sdk.claims.ClaimsTransport;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


/**
 * The password grant handler implementation.
 */
class Handler {


	/**
	 * The logger.
	 */
	private static final Logger LOG = LogManager.getLogger("MAIN");


	/**
	 * The API access token.
	 */
	private final BearerAccessToken apiAccessToken;


	/**
	 * The LDAP directory host name / IP address.
	 */
	private final String ldapHost;


	/**
	 * The LDAP directory port.
	 */
	private final int ldapPort;


	/**
	 * The template for resolving usernames to LDAP DNs.
	 */
	private final String ldapDnTemplate;


	/**
	 * The allowed scope values for successfully authenticated users.
	 */
	private final Scope allowedScopeValues;


	/**
	 * Creates a new password grant handler.
	 *
	 * @param apiAccessToken     The API access token. Must not be
	 *                           {@code null}.
	 * @param ldapHost           The LDAP directory host name / IP address.
	 *                           Must not be {@code null}.
	 * @param ldapPort           The LDAP directory port. Must be in the
	 *                           valid port in the range from 1 to 65535.
	 * @param ldapDnTemplate     The template for resolving usernames to
	 *                           LDAP DNs. Must not be {@code null}.
	 * @param allowedScopeValues The allowed scope values for successfully
	 *                           authenticated users. Must not be
	 *                           {@code null}.
	 */
	public Handler(final BearerAccessToken apiAccessToken,
		       final String ldapHost,
		       final int ldapPort,
		       final String ldapDnTemplate,
		       final Scope allowedScopeValues) {

		if (apiAccessToken == null)
			throw new IllegalArgumentException("The API access token must not be null");

		this.apiAccessToken = apiAccessToken;

		if (ldapHost == null)
			throw new IllegalArgumentException("The LDAP host must not be null");

		this.ldapHost = ldapHost;

		if (ldapPort < 1 || ldapPort > 65535)
			throw new IllegalArgumentException("The LDAP port must be in the range from 1 to 65535");

		this.ldapPort = ldapPort;

		if (ldapDnTemplate == null)
			throw new IllegalArgumentException("The LDAP DN template must not be null");

		this.ldapDnTemplate = ldapDnTemplate;


		if (allowedScopeValues == null)
			throw new IllegalArgumentException("The allowed scope values must not be null");

		this.allowedScopeValues = allowedScopeValues;
	}


	/**
	 * Verifies the HTTP request for having a valid API access token.
	 *
	 * @param httpRequest The HTTP request. Must not be {@code null}.
	 *
	 * @throws GeneralException If the HTTP request was not authorised.
	 */
	public void verifyAPIAccessToken(final HTTPRequest httpRequest)
		throws GeneralException {

		BearerAccessToken receivedToken = BearerAccessToken.parse(httpRequest);

		if (! apiAccessToken.equals(receivedToken)) {
			String msg = "Invalid API access token";
			LOG.error(msg);
			throw new GeneralException(msg, BearerTokenError.INVALID_TOKEN);
		}
	}


	/**
	 * Verifies the specified credentials against the LDAP directory.
	 *
	 * @param username The username. Must not be {@code null}.
	 * @param password The password. Must not be {@code null}.
	 *
	 * @throws GeneralException If the credentials are bad, or verification
	 *                          failed.
	 */
	public void verifyResourceOwnerCredentials(final String username, final Secret password)
		throws GeneralException {

		LDAPConnection ldapConnection;

		try {
			ldapConnection = new LDAPConnection(ldapHost, ldapPort);

		} catch (LDAPException e) {
			String msg = "Couldn't connect to LDAP server: " + e.getMessage();
			LOG.error(msg);
			throw new GeneralException(msg, OAuth2Error.SERVER_ERROR.appendDescription(": " + msg));
		}

		// Compose the DN, may use LDAP search instead
		String dn = ldapDnTemplate.replace("%u", username);

		LOG.info("Verifying LDAP password for {} ...", dn);

		try {
			ldapConnection.bind(dn, password.getValue());

		} catch (LDAPException e) {

			String msg = "Couldn't verify LDAP password for user DN " + dn + ": " + e.getMessage();
			LOG.error(msg);

			if (e.getResultCode().equals(ResultCode.INVALID_CREDENTIALS)) {
				// Bad username and / or password
				LOG.info("Bad username / password");
				throw new GeneralException(msg, OAuth2Error.INVALID_GRANT);
			} else {
				// Other error
				throw new GeneralException(msg, OAuth2Error.SERVER_ERROR.appendDescription(": " + msg));
			}

		} finally {

			 ldapConnection.close();
		}

		LOG.info("Username / password verified");
	}


	/**
	 * Processes an HTTP request with an encoded password grant handler
	 * request.
	 *
	 * @param httpRequest The HTTP request. Must not be {@code null}.
	 *
	 * @return The HTTP response.
	 */
	public HTTPResponse process(final HTTPRequest httpRequest) {

		PasswordGrantHandlerRequest handlerRequest;
		try {
			handlerRequest = PasswordGrantHandlerRequest.parse(httpRequest);
		} catch (ParseException e) {
			String msg = "Invalid password grant handler request: " + e.getMessage();
			LOG.info(msg);
			var errorResponse = new HTTPResponse(HTTPResponse.SC_BAD_REQUEST);
			errorResponse.setEntityContentType(ContentType.APPLICATION_JSON);
			errorResponse.setBody(OAuth2Error.INVALID_REQUEST.appendDescription(": " + msg).toJSONObject().toJSONString());
			return errorResponse;
		}

		PasswordGrantAuthorization authz;
		try {
			verifyAPIAccessToken(httpRequest);
			authz = process(handlerRequest);

		} catch (GeneralException e) {

			HTTPResponse errorResponse;
			if (e.getErrorObject() != null) {
				errorResponse = new HTTPResponse(e.getErrorObject().getHTTPStatusCode());
				errorResponse.setEntityContentType(ContentType.APPLICATION_JSON);
				errorResponse.setBody(e.getErrorObject().toJSONObject().toJSONString());
				if (e.getErrorObject() instanceof BearerTokenError bte) {
					errorResponse.setWWWAuthenticate(bte.toWWWAuthenticateHeader());
				}
			} else {
				errorResponse = new HTTPResponse(HTTPResponse.SC_SERVER_ERROR);
				errorResponse.setEntityContentType(ContentType.APPLICATION_JSON);
				errorResponse.setBody(OAuth2Error.SERVER_ERROR.appendDescription(": " + e.getMessage()).toJSONObject().toJSONString());
			}

			return errorResponse;
		}

		var successResponse = new HTTPResponse(HTTPResponse.SC_OK);
		successResponse.setEntityContentType(ContentType.APPLICATION_JSON);
		successResponse.setBody(authz.toJSONObject().toJSONString());
		return successResponse;
	}


	/**
	 * Processes a parsed password grant handler request.
	 *
	 * @param request The password grant handler request. Must not be
	 *                {@code null}.
	 *
	 * @return The password grant authorisation on success.
	 *
	 * @throws GeneralException If the grant was denied or processing
	 *                          failed.
	 */
	public PasswordGrantAuthorization process(final PasswordGrantHandlerRequest request)
		throws GeneralException {

		// Verify the username and password first
		verifyResourceOwnerCredentials(request.getGrant().getUsername(), request.getGrant().getPassword());

		// Set the allowed scope values
		Scope scope;

		if (request.getTokenRequestParameters().getScope() == null) {
			// Default to some value
			scope = allowedScopeValues;
		} else {
			// Retain only the allowed scope values
			scope = request.getTokenRequestParameters().getScope();
			scope.retainAll(allowedScopeValues);
		}

		// Compose the authorisation response for the Connect2id server
		var subject = new Subject(request.getGrant().getUsername()); // Can be a different ID
		var authTime = new Date(); // Optional authentication time
		var acr = new ACR("1"); // Some optional authentication level reference
		List<AMR> amrList = Collections.singletonList(new AMR("ldap")); // Some optional auth methods
		List<Audience> audienceList = null; // May specify explicit audience for the access token (if JWT-encoded)
		boolean longLived = false; // Whether the authorisation should be persisted with the Connect2id server
		var accessTokenSpec = new AccessTokenSpec(
			600L, // The access token lifetime, in seconds
			null,
			TokenEncoding.SELF_CONTAINED, // The preferred access token encoding (JWT)
			Optional.empty() // Whether the access token should be encrypted after signing
		);
		var refreshTokenSpec = new RefreshTokenSpec(
			false, // Whether to issue a refresh token (requires long-lived authz)
			0L // The default refresh token lifetime
		);
		var idTokenSpec = new IDTokenSpec(
			true, // Whether to issue an OpenID Connect ID token
			600L, // Specify a 10 minutes lifetime for the ID token
			null // Optional impersonated subject for the ID token
		);

		Set<String> claims = new HashSet<>(); // Optional UserInfo claims to release, based on the allowed scope

		for (Scope.Value sv: scope) {

			if (sv.equals(OIDCScopeValue.OPENID))
				claims.addAll(OIDCScopeValue.OPENID.getClaimNames());

			if (sv.equals(OIDCScopeValue.EMAIL))
				claims.addAll(OIDCScopeValue.EMAIL.getClaimNames());

			if (sv.equals(OIDCScopeValue.PROFILE))
				claims.addAll(OIDCScopeValue.PROFILE.getClaimNames());

			if (sv.equals(OIDCScopeValue.PHONE))
				claims.addAll(OIDCScopeValue.PHONE.getClaimNames());

			if (sv.equals(OIDCScopeValue.ADDRESS))
				claims.addAll(OIDCScopeValue.ADDRESS.getClaimNames());
		}

		var claimsSpec = new ClaimsSpec(
			claims, // The names of the claims to release
			null, // Optional claims locales
			null, // Optional preset ID token claims
			null, // Optional preset UserInfo claims
			ClaimsTransport.USERINFO // The preferred claims transport
		);

		return new PasswordGrantAuthorization(
			subject,
			scope,
			longLived,
			accessTokenSpec,
			refreshTokenSpec,
			idTokenSpec,
			claimsSpec,
			null // Optional data
		);
	}
}
